package com.huliang.cxf.ssl.helloword;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 
 * 
 * @author huliang
 * @version $Id: WSServicesContextListener.java, v 0.1 2013年8月13日 上午9:27:37 huliang Exp $
 */
public class WSServicesContextListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent context) {
    }

    @Override
    public void contextInitialized(ServletContextEvent context) {
        System.out.println("content initialized");
        System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
        System.setProperty("sun.security.ssl.allowLegacyHelloMessages", "true");
        System.setProperty("javax.net.debug", "all");
    }

}
